#ifndef FUNCTIONAL_H
#define FUNCTIONAL_H

#include <QString>
#include <QStringList>
#include <algorithm>


class Functional
{
public:
    Functional();
    int getRowNumber(QString str);
    int getEmptyRowNumber(QString str);
    int getCommentNumber(QString str);
    int getUncommentRowNumber(QString str);
    int getOperandVacebulary(QString str);
    int getOperandVacebulary2(QString str);
    int getOperandNumber(QString str);
    int getAllOperand(QString str);
    int getAllOperand1(QString str);
    int getCiclic(QString str);
    int getR(QString str);
};

#endif // FUNCTIONAL_H
