#include "mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->functional = new Functional();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete functional;
}

//���������� ����� ����
void MainWindow::on_SLOC_clicked()
{
    int num = this->functional->getRowNumber(ui->plainTextEdit->toPlainText());
    ui->label->setText(QString::number(num+1));
}

// ���������� ������ �����
void MainWindow::on_EmptyLines_clicked()
{
    int empty_num = this->functional->getEmptyRowNumber(ui->plainTextEdit->toPlainText());
    ui->label_2->setText(QString::number(empty_num+1));
}

//���������� ������������
void MainWindow::on_Comments_clicked()
{
        QString str=ui->plainTextEdit->toPlainText();
        int comment_number = this->functional->getCommentNumber(str);
        ui->label_3->setText(QString::number(comment_number));
        int num = this->functional->getRowNumber(str);
        ui->label_5->setText(QString::number(((float)comment_number)/(num+1)*100)+"%");
}

//���������� ����� ����(���-�� ����� �� ������������ � ����������� - ���������� ������ �����)
void MainWindow::on_OnlyCode_clicked()
{
    QString str=ui->plainTextEdit->toPlainText();
    int uncomment = this->functional->getUncommentRowNumber(str);
    int empty = this->functional->getEmptyRowNumber(str);
    ui->label_4->setText(QString::number(uncomment-empty));
}

//������� ���������
void MainWindow::on_VocOperandn2_clicked()
{
    int num = this->functional->getOperandVacebulary2(ui->plainTextEdit->toPlainText());
    ui->label_6->setText(QString::number(num));
}

//����� ���-�� ���������
void MainWindow::on_AllOperandN2_clicked()
{
    int num = this->functional->getAllOperand(ui->plainTextEdit->toPlainText());
    ui->label_9->setText(QString::number(num));
}

//������� ����������
void MainWindow::on_VocOperatn1_clicked()
{
    QString str=ui->plainTextEdit->toPlainText();
    ui->label_7->setText(QString::number(this->functional->getOperandVacebulary(str)));
}

//����� ���-�� ����������
void MainWindow::on_AllOperatN1_clicked()
{
    QString str=ui->plainTextEdit->toPlainText();
    ui->label_8->setText(QString::number(this->functional->getAllOperand1(str)));
}

//��������������� ���������
void MainWindow::on_Cyclomatic_clicked()
{
    QString str=ui->plainTextEdit->toPlainText();
    ui->label_10->setText(QString::number(this->functional->getCiclic(str)));
}

//������� ���������
void MainWindow::on_Voc_clicked()
{
    QString str = ui->plainTextEdit->toPlainText();
    ui->label_11->setText(QString::number(this->functional->getOperandVacebulary(str)+this->functional->getOperandVacebulary2(str)-this->functional->getR(str)));
}

//����� ���������
void MainWindow::on_N_clicked()
{
    QString str = ui->plainTextEdit->toPlainText();
    int N = this->functional->getAllOperand(str) + this->functional->getAllOperand1(str) + 2;
    ui->label_12->setText(QString::number(N));
}

//����� ���������
void MainWindow::on_V_clicked()
{
    QString str = ui->plainTextEdit->toPlainText();
    int N = this->functional->getAllOperand(str) + this->functional->getAllOperand1(str) + 2;
    int m = this->functional->getOperandVacebulary(str)+this->functional->getOperandVacebulary2(str)-this->functional->getR(str);
    float q=N*qLn(m)/qLn(2);
    ui->label_13->setText(QString::number(q));

}

//���������� ��������� �� ������ � ���������� �������� ����������
void MainWindow::on_Cl_clicked()
{
    QString str=ui->plainTextEdit->toPlainText();
    ui->label_14->setText(QString::number(str.count("if(")));
}
